#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp

form = """
    <form action= " " method="POST"> 
    <input type="text" name="content">
    <input type="submit" value="Enviar"></form>
"""

class contentApp(webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        meth = request.split(' ', 1)[0]  # GET resource HTTP/1.1
        res = request.split(' ', 2)[1]
        body = request.split('\n', 2)[-1]

        return meth, res, body

    def process(self, resourceName):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        meth, res, body = resourceName

        if meth == "GET":

            httpCode = "200 OK"
            if res in self.content.keys():
                htmlBody = "<html><body>Content: " + self.content[res] \
                           + "<br><br>You can update the content using this form" \
                           + form + "</body></html>"
            else:
                htmlBody = "<html><body>" + form + "</body></html>"

            return httpCode, htmlBody

        if meth == "POST":

            newbody = body.split("\n")[-1]
            if newbody[0:7] == "content":
                self.content[res] = body.split("=")[-1]
            else:
                self.content[res] = newbody

            httpCode = "200 OK"
            htmlBody = "<html><body>Content: " + self.content[res] \
                       + "<br><br>Dict has been up-to-date</body></html>"

            return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
